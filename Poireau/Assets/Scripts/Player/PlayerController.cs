﻿using UnityEngine;

[RequireComponent(typeof(PlayerActor))]
public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    PlayerActor actor;
    WeaponManager weapon_manager;
    void Start()
    {
        actor = GetComponent<PlayerActor>();
        weapon_manager = GetComponent<WeaponManager>();
    }

    // Update is called once per frame
    void Update()
    {
        InteractionStep();
        

        if (Input.GetKeyDown(KeyCode.LeftControl) == true)
        {
            actor.InflictDamage(1, actor);
        }

        if (Input.GetKey(KeyCode.Space) == true)
        {
            weapon_manager.Shoot(actor);
        }
    }

    void InteractionStep()
    {
        InteractionableBase ib;
        if (CheckInteraction(out ib))
        {
            HandleInteraction(ib);
        }
        else
        {
            actor.interactionHUD.ClearAllInteraction();
        }
    }

    bool CheckInteraction(out InteractionableBase ib)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 2);

        ib = null;

        foreach (Collider collider in colliders)
        {
            InteractionableBase collider_ib = collider.gameObject.GetComponent<InteractionableBase>();
            if (collider_ib)
            {
                ib = collider_ib;
                break;
            }
        }

        return ib != null;
    }

    void HandleInteraction(InteractionableBase interactionBase)
    {
        if (interactionBase)
        {
            actor.interactionHUD.simpleText.SetSimpleText(interactionBase.interactionText);

            if (Input.GetKeyDown(interactionBase.k_keyToPress) == true)
            {
                interactionBase.Interract();
            }
        }
    }
}