﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectActor))]
public class DamageArea : MonoBehaviour
{
    public int damage = 1;
    public float perSecond = 1f;

    public static Dictionary<Actor, float> timers = new Dictionary<Actor, float>();

    ObjectActor actor;

    private void Start()
    {
        actor = GetComponent<ObjectActor>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Actor actor = other.gameObject.GetComponent<Actor>();
        if (actor && !timers.ContainsKey(actor))
        {
            timers.Add(actor, perSecond);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Actor other_actor = other.gameObject.GetComponent<Actor>();
        if (other_actor)
        {
            float timer;
            timers.TryGetValue(other_actor, out timer);
            if (timer <= 0)
            {
                other_actor.InflictDamage(damage, actor);
                timer = perSecond;
            }

            timer -= Time.deltaTime;
            timers[other_actor] = timer;
        }
    }
}
