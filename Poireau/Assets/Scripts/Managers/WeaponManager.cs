﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public int maxWeapon = 3;
    public float speedChangeWeapon = 0.3f;
    public List<WeaponController> initialWeapons;

    public int actualWeaponIndex { get; private set; }
    public List<WeaponController> weapons { get; private set; }

    public Transform weaponHoldingPoint { get; private set; }
    protected Vector3 initialWeaponHoldingPoint;
    protected WeaponController actualWeapon;

    public bool canShoot {get; private set;}

    protected virtual void Start()
    {
        canShoot = true;
        actualWeaponIndex = 0;
        weaponHoldingPoint = transform.Find("WeaponHoldingPoint");
        initialWeaponHoldingPoint = weaponHoldingPoint.localPosition;
        weapons = initialWeapons;
        if (weapons.Count > 0) InstantiateNewWeapon(GetWeapon(0));
    }

    protected virtual void Update()
    {
        if (!canShoot)
        {
            weaponHoldingPoint.transform.localRotation = Quaternion.RotateTowards(weaponHoldingPoint.transform.localRotation, Quaternion.identity, 1/speedChangeWeapon);
            if (weaponHoldingPoint.transform.localRotation == Quaternion.identity) canShoot = true;
        }
    }

    bool HasMaxWeaponReached()
    {
        if (weapons.Count >= maxWeapon) return true;
        return false;
    }

    void NextWeaponIndex()
    {
        actualWeaponIndex = (actualWeaponIndex + 1) % weapons.Count;
    }

    void PreviousWeaponIndex()
    {
        if (actualWeaponIndex == 0) actualWeaponIndex = weapons.Count - 1;
        else actualWeaponIndex = Mathf.Abs((actualWeaponIndex - 1) % weapons.Count);
    }

    WeaponController GetWeapon(int index)
    {
        return weapons[index];
    }

    public void ClearWeapons()
    {
        weapons.Clear();
    }

    public bool CanGetWeapon(WeaponController weapon, out WeaponController removedWeapon)
    {
        removedWeapon = null;
        foreach (WeaponController weaponController in weapons)
        {
            if (weaponController.weaponName == weapon.weaponName) return false;
        }
        if (HasMaxWeaponReached()) removedWeapon = GetActualWeapon();
        return true;
    }

    public bool CanGetWeapon(WeaponController weapon)
    {
        return CanGetWeapon(weapon, out _);
    }

    public void PickWeapon(WeaponController weapon)
    {
        if (weapons.Count < maxWeapon)
        {
            AddWeapon(weapon);
            actualWeaponIndex = weapons.Count - 1;
            WeaponAt(actualWeaponIndex);
        }
        else {
            ChangeActualWeapon(weapon);
            WeaponAt(actualWeaponIndex);
        } 
    }

    public void ChangeActualWeapon(WeaponController weapon)
    {
        //WeaponController actualWc = weapons[actualWeaponIndex];
        //Collectable collectable = actualWc.GetComponent<Collectable>();
        //if (collectable != null) ResourceManager.instance.InstantiateNewItemCollectable(collectable, transform.position);
        weapons[actualWeaponIndex] = weapon;
    }

    public bool AddWeapon(WeaponController weapon)
    {
        if (weapons.Count >= maxWeapon) return false;
        weapons.Add(weapon);
        if (weapons.Count == 1) {
            InstantiateNewWeapon(GetActualWeapon());
        };
        return true;
    }

    public bool RemoveWeapon(WeaponController weapon)
    {
        return weapons.Remove(weapon); ;
    }

    public bool RemoveActualWeapon()
    {
        if (weapons.Count == 0) return false;
        weapons.RemoveAt(actualWeaponIndex);
        return true;
    }

    public WeaponController GetActualWeapon()
    {
        return actualWeapon;
    }

    WeaponController GetNextWeapon()
    {
        if (weapons.Count <= 1) return null;
        NextWeaponIndex();
        return GetWeapon(actualWeaponIndex);
    }

    WeaponController GetPreviousWeapon()
    {
        if (weapons.Count <= 1) return null;
        PreviousWeaponIndex();
        return GetWeapon(actualWeaponIndex);
    }

    virtual protected void InstantiateNewWeapon(WeaponController newWeapon)
    {
        if (newWeapon == null) return;
        canShoot = false;
        if (actualWeapon) Destroy(actualWeapon.gameObject);
        GameObject weaponObject = Instantiate(newWeapon.gameObject, weaponHoldingPoint.transform, false);
        weaponHoldingPoint.transform.Rotate(-90, 0, 0, Space.Self);
        actualWeapon = weaponObject.GetComponent<WeaponController>();
    }

    public virtual void NextWeapon()
    {
        WeaponController wc = GetNextWeapon();
        if (!wc) return;
        InstantiateNewWeapon(wc);
    }

    public virtual void PreviousWeapon()
    {
        WeaponController wc = GetPreviousWeapon();
        if (!wc) return;
        InstantiateNewWeapon(wc);
    }

    public void WeaponAt(int index)
    {
        WeaponController wc = GetWeapon(index);
        if (!wc) return;
        InstantiateNewWeapon(wc);
    }

    virtual public void Shoot(Actor fromActor, Vector3? aimPosition = null)
    {
        if (actualWeapon == null || !canShoot) return;
        actualWeapon.Shoot(fromActor, aimPosition);
    }
}
