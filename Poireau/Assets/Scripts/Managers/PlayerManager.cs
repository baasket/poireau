﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static Dictionary<int, PlayerActor> players = new Dictionary<int, PlayerActor>();
    
    public static int Register(PlayerActor player)
    {
        players.Add(player.id, player);
        return player.id;
    }

    public static PlayerActor Get(int player_id)
    {
        PlayerActor player;
        if(!players.TryGetValue(player_id, out player))
        {
            Debug.LogError("The player " + player_id + " doesn't exist!");
        }
        return player;
    }
}
