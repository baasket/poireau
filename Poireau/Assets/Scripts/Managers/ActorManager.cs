﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorManager : MonoBehaviour
{
    static List<Actor> actors = new List<Actor>();

    public static void AddActor(Actor actor)
    {
        if (!actors.Contains(actor))
        {
            actors.Add(actor);
        }
    }

    public static void RemoveActor(Actor actor)
    {
        actors.Remove(actor);
    }

    public static Actor[] GetActors()
    {
        return actors.ToArray();
    }
}
