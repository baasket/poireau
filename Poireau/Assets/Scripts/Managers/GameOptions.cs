﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameManager
{
    public static bool friendlyFire;
    public static int iron_start_number;
    public static int wood_start_number;
    public static int beer_start_number;
    public static int levelState;
    public static GameConstants.Difficulties difficulty;

    public static bool IsLevelRunning()
    {
        return levelState == GameConstants.s_LevelRunning;
    }
}

public class GameOptions : MonoBehaviour
{
    public bool friendlyFire = false;
    public int iron_start_number = 0;
    public int wood_start_number = 0;
    public int beer_start_number = 0;

    public GameConstants.Difficulties difficulty;
    private void Awake()
    {
        GameManager.friendlyFire = friendlyFire;
        GameManager.iron_start_number = iron_start_number;
        GameManager.wood_start_number = wood_start_number;
        GameManager.beer_start_number = beer_start_number;
        GameManager.friendlyFire = friendlyFire;
        GameManager.levelState = GameConstants.s_LevelRunning;
        GameManager.difficulty = difficulty;
    }

}
