﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponManager : WeaponManager
{
    public float showSelectorDuration = 2f;

    protected override void Start()
    {
        base.Start();
    }

    protected override void InstantiateNewWeapon(WeaponController newWeapon)
    {
        base.InstantiateNewWeapon(newWeapon);
        RefreshAmmoHUD();
    }

    public override void NextWeapon()
    {
        base.NextWeapon();
        HandleSelector();
    }

    public override void PreviousWeapon()
    {
        base.PreviousWeapon();
        HandleSelector();
    }

    public void HandleSelector()
    {
        //weaponSelector.ShowSelector(this);
    }


    override public void Shoot(Actor fromActor, Vector3? aimPosition = null)
    {
        base.Shoot(fromActor, aimPosition);
        RefreshAmmoHUD();
    }

    private void RefreshAmmoHUD()
    {
        //if (base.actualWeapon.isInfiniteAmmo) playerHUDAmmo.Refresh(-1, -1);
        //else playerHUDAmmo.Refresh(base.actualWeapon.maxAmmo, base.actualWeapon.GetCurrentAmmo());
    }
}
