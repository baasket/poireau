﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;

    public BaseResourcesHUD hud_iron;
    public BaseResourcesHUD hud_wood;
    public BaseResourcesHUD hud_beer;

    int iron_quantity;
    int wood_quantity;
    int beer_quantity;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        iron_quantity = GameManager.iron_start_number;
        wood_quantity = GameManager.wood_start_number;
        beer_quantity = GameManager.beer_start_number;
        RefreshResourcesHUD();
    }

    public void IncrementIron(int i = 1)
    {
        iron_quantity += i;
        RefreshResourcesHUD();
    }

    public void IncrementWood(int i = 1)
    {
        wood_quantity += i;
        RefreshResourcesHUD();
    }

    public void IncrementBeer(int i = 1)
    {
        beer_quantity += i;
        RefreshResourcesHUD();
    }

    void RefreshResourcesHUD()
    {
        hud_iron.DisplayResource("iron", iron_quantity);
        hud_wood.DisplayResource("wood", wood_quantity);
        hud_beer.DisplayResource("beer", beer_quantity);
    }
}
