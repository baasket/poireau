﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [Header("Stats")]
    [Tooltip("Damage done by this gun")]
    public float damage = 1f;
    [Tooltip("Number of shoot possible per second")]
    public float attackSpeed = 1f;
    [Tooltip("Speed to give to the projectile")]
    public float projectileSpeed = 20f;
    [Tooltip("Number of projectile per shoot")]
    public int numberOfProjectile = 1;
    [Tooltip("Spread in angle to add to each projectile")]
    public float projectileSpreadAngle = 0f;
    [Tooltip("Name of the weapon")]
    public string weaponName = "";
    [Tooltip("Max ammo this gun can handle")]
    public int maxAmmo;
    [Tooltip("Is infinite ammo?")]
    public bool isInfiniteAmmo = false;
    [Tooltip("Number of ammo this gun will be spawn with")]
    public int startAmmo;

    [Header("Recoil")]
    [Tooltip("Strength of the recoil")]
    public float recoilStrength = 0.2f;
    [Tooltip("Speed of the recoil")]
    public float recoilSpeed = 2f;
    [Tooltip("Speed after the recoil to come back in it original position")]
    public float resetSpeed = 1f;
    [Tooltip("Maximum recoil of the gun")]
    public float maxRecoil = 0.3f;

    [Header("Aim stats")]
    [Tooltip("Spread in angle to add to each projectile when aiming")]
    public float aimProjectileSpreadAngle = 0f;

    [Header("Aim Recoil")]
    [Tooltip("Strength of the recoil when aiming")]
    public float aimRecoilStrength = 0.2f;
    [Tooltip("Speed of the recoil when aiming")]
    public float aimRecoilSpeed = 2f;
    [Tooltip("Speed after the recoil to come back in it original position when aiming")]
    public float aimResetSpeed = 1f;

    [Header("References")]
    [Tooltip("Reference to the projectile prefab")]
    public ProjectileBase projectilePrefab;

    AudioSource audioSource;
    ParticleSystem firePointParticles;
    float attackTimer = 0f;
    float recoil = 0f;
    bool isRecoiling = false;
    int currentAmmo;

    Vector3 initialWeaponPosition;

    Transform firePoint;
    public Transform aimPoint { get; private set; }

    private void OnEnable()
    {
        currentAmmo = startAmmo;
    }

    void Start()
    {
        firePoint = transform.Find("FirePoint");
        firePointParticles = firePoint.GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
        aimPoint = transform.Find("AimPoint");
    }

    void Update()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        }
        HandleRecoil();
    }

    virtual public void Shoot(Actor fromActor, Vector3? aimPosition = null)
    {
        if (attackTimer > 0) return;
        else if (attackTimer <= 0)
            attackTimer = 1 / (attackSpeed * fromActor.attackSpeedMultiplier);
        if (!isInfiniteAmmo)
        {
            if (currentAmmo <= 0) return;
            currentAmmo -= 1;
        }
        if (audioSource) audioSource.Play();
        if (aimPosition != null) transform.LookAt(aimPosition.Value);
        for (int i = 0; i < numberOfProjectile; i++)
        {
            ShootProjectile(fromActor);
        }
        if (firePointParticles != null) firePointParticles.Play();
    }

    private void ShootProjectile(Actor fromActor)
    {
        PlayerActor playerActor = fromActor as PlayerActor;
        Vector3 projectileDirection = GetShotDirectionWithinSpread(firePoint.transform, playerActor);
        Quaternion projectileRotation = Quaternion.LookRotation(projectileDirection);
        ProjectileBase projectileObject = Instantiate(projectilePrefab, firePoint.transform.position, projectileRotation);
        projectileObject.Shoot(this, fromActor, projectileSpeed);
        SetRecoil();
    }

    Vector3 GetShotDirectionWithinSpread(Transform shootTransform, PlayerActor playerActor = null)
    {
        float tmpSpreadAngle = projectileSpreadAngle;
        float spreadAngleRatio = tmpSpreadAngle / 180f;
        Vector3 spreadWorldDirection = Vector3.Slerp(shootTransform.forward, Random.insideUnitSphere, spreadAngleRatio);

        return spreadWorldDirection;
    }

    void SetRecoil()
    {
        if (!isRecoiling) initialWeaponPosition = transform.localPosition;
        recoil = recoilStrength;
        isRecoiling = true;
    }

    public void HandleAim()
    {
        isRecoiling = false;
    }

    public void ResetAim()
    {
        isRecoiling = false;
        transform.localPosition = Vector3.zero;
    }

    void HandleRecoil()
    {
        if (!isRecoiling) return;
        if (recoil > 0f)
        {
            float x = transform.localPosition.x;
            float y = transform.localPosition.y;
            float z = Mathf.Clamp(transform.localPosition.z - maxRecoil, initialWeaponPosition.z - maxRecoil, initialWeaponPosition.z);
            Vector3 recoilTarget = new Vector3(x, y, z);
            transform.localPosition = Vector3.Lerp(transform.localPosition, recoilTarget, Time.deltaTime * recoilSpeed);
            recoil -= Time.deltaTime;
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, initialWeaponPosition, Time.deltaTime * resetSpeed);
        }
        if (transform.localPosition == initialWeaponPosition) isRecoiling = false;
    }

    public int GetCurrentAmmo()
    {
        return currentAmmo;
    }
}
