﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractionableBase : MonoBehaviour
{
    public abstract string hud_action { get; }
    public abstract KeyCode k_keyToPress { get; }

    public string interactionText { get => "Press <" + k_keyToPress.ToString() + "> to " + hud_action; }

    public abstract void Interract();
}
