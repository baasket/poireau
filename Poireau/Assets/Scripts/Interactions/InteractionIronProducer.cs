﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionIronProducer : InteractionableBase
{
    public override string hud_action { get => "produce iron"; }
    public override KeyCode k_keyToPress { get => KeyCode.H; }

    public override void Interract()
    {
        ResourceManager.instance.IncrementIron();
    }
}
