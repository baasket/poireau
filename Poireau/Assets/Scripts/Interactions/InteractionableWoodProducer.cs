﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionableWoodProducer : InteractionableBase
{
    public override string hud_action { get => "produce wood"; }
    public override KeyCode k_keyToPress { get => KeyCode.H; }

    public override void Interract()
    {
        ResourceManager.instance.IncrementWood(5);
    }
}
