﻿public class GameConstants
{
    // all the constant string used across the game
    public const string k_AxisNameVertical = "Vertical";
    public const string k_AxisNameHorizontal = "Horizontal";
    public const string k_MouseAxisNameVertical = "Mouse Y";
    public const string k_MouseAxisNameHorizontal = "Mouse X";
    public const string k_ButtonNameFire = "Fire";
    public const string k_ButtonNameSprint = "Sprint";
    public const string k_ButtonNameSwitchWeapon = "Mouse ScrollWheel";
    public const string k_ButtonNamePauseMenu = "Pause Menu";
    public const string k_ButtonNameSubmit = "Submit";
    public const string k_ButtonNameCancel = "Cancel";
    public const string k_ButtonNameInteract = "Interact";

    // Level states
    public const int s_LevelRunning = 0;
    public const int s_LevelWin = 1;
    public const int s_LevelFail = 2;

    // Difficulties
    public enum Difficulties
    {
        Easy,
        Normal,
        Hard
    };

    // Resource types
    public enum ResourceType
    {
        iron,
        beer,
        wood
    };

    // Actor type
    public enum ActorType
    {
        Player,
        GroundEnemy,
        Object
    };
}
