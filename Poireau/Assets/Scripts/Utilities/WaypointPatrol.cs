﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class WaypointPatrol : MonoBehaviour
{
    bool isPatrol = false;
    public Transform[] waypoints;

    NavMeshAgent navMeshAgent;

    int currentWaypointIndex = 0;
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        StartPatrol();
    }

    public void StopPatrol()
    {
        if (!isPatrol) return;
        isPatrol = false;
        navMeshAgent.ResetPath();
    }

    public void StartPatrol()
    {
        if (isPatrol || waypoints.Length == 0) return;
        isPatrol = true;
        navMeshAgent.SetDestination(waypoints[currentWaypointIndex].position);
    }

    void Update()
    {
        if (!isPatrol) return;
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[currentWaypointIndex].position);
        }
    }
}
