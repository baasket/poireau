﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(ActorObserver))]
public class EnemyGroundController : AbstractAIController
{
    public float rotationSpeed = 10f;
    public float attackDistance = 5f;

    public float rememberTime = 5f;
    float rememberTimer = 0f;

    WaypointPatrol patrolScript;
    NavMeshAgent navMeshAgent;
    WeaponController weaponController;
    Transform viewPoint;
    Actor actor;
    ActorObserver actorObserver;
    Vector3 positionBeforeFocus;
    Actor focusActor;

    bool isFocusing = false;

    private void Awake()
    {
        patrolScript = GetComponent<WaypointPatrol>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        weaponController = GetComponentInChildren<WeaponController>();
        actor = GetComponent<Actor>();
        viewPoint = transform.Find("ViewPoint");
        actorObserver = GetComponent<ActorObserver>();
    }

    void Update()
    {
        if (!GameManager.IsLevelRunning()) enabled = false;
        if (!isFocusing)
        {
            Actor otherActor = actorObserver.FindNearestActor();
            if (otherActor != null)
            {
                FocusActor(otherActor);
            }
        }
        else
        {
            if (focusActor == null || rememberTimer <= 0) StopFocus();
            else if (actorObserver.DoSeeActor(focusActor)) FocusActor(focusActor);
            else
            {
                rememberTimer -= Time.deltaTime;
                navMeshAgent.SetDestination(focusActor.transform.position);
            }
        }
    }

    bool IsActorInAttackRange(Actor actorFocused)
    {
        float sqrDistance;
        return actorObserver.DoSeeActor(actorFocused, out sqrDistance) && sqrDistance <= attackDistance * attackDistance;
    }

    void RememberActor(Actor actorToRemember)
    {
        rememberTimer = rememberTime;
        focusActor = actorToRemember;
    }

    public override void FocusActor(Actor actorFocused)
    {
        if (actorFocused == null || !actorFocused.isAttackable) return;
        if (!isFocusing) positionBeforeFocus = transform.position;
        isFocusing = true;
        RememberActor(actorFocused);
        if (patrolScript) patrolScript.StopPatrol();
        if (IsActorInAttackRange(actorFocused))
        {
            navMeshAgent.ResetPath();
            Vector3 actorHitPosition = actorObserver.GetActorHitPosition(actorFocused);
            Vector3 lookAtPosition = actorHitPosition - viewPoint.position;
            lookAtPosition.y = transform.position.y;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAtPosition), rotationSpeed * Time.deltaTime);
            weaponController.Shoot(actor, actorHitPosition);
        }
        else
        {
            navMeshAgent.SetDestination(actorFocused.transform.position);
        }
    }

    void StopFocus()
    {
        if (isFocusing)
        {
            navMeshAgent.ResetPath();
            navMeshAgent.SetDestination(positionBeforeFocus);
            if (patrolScript) patrolScript.StartPatrol();
            rememberTimer = 0;
            isFocusing = false;
        }
    }
}
