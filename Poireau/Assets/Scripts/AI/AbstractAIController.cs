﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAIController : MonoBehaviour
{
    public abstract void FocusActor(Actor actorFocused);
}
