﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCollectable : MonoBehaviour
{
    public Collectable collectable;
    public float rotateSpeed = 5f;

    GameObject collectableGameObject;
    Transform collectablePoint;

    private void Start()
    {
        collectablePoint = transform.Find("CollectablePoint");
        collectableGameObject = Instantiate(collectable.gameObject, collectablePoint, false);
    }

    private void Update()
    {
        collectableGameObject.transform.Rotate(Vector3.up, rotateSpeed);
    }

    public virtual void DestroyCollectable()
    {
        Destroy(gameObject);
    }
}
