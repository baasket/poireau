﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileInstant : MonoBehaviour
{
    [Header("General")]
    [Tooltip("VFX prefab to spawn upon impact")]
    public GameObject impactVFX;
    [Tooltip("Offset along the hit normal where the VFX will be spawned")]
    public float impactVFXSpawnOffset = 0.1f;
    [Tooltip("Clip to play on impact")]
    public AudioClip impactSFXClip;
    [Tooltip("Material of the projectile's line")]
    public Material lineMaterial;
    [Tooltip("Layers this projectile can collide with")]
    public LayerMask hittableLayers = -1;
    [Tooltip("Time before the line is destroy")]
    public float timeBeforeDestroy = 0.1f;
    [Tooltip("Distance that the line will be draw for a projectile that doesn't colide with anything")]
    public float maxDistanceForInfiniteProjectile = 100f;

    ProjectileBase m_ProjectileBase;
    Transform particlesTransform;
    Vector3 m_RootPosition;
    List<Collider> m_IgnoredColliders;
    LineRenderer lineRenderer;

    float timer;

    const QueryTriggerInteraction k_TriggerInteraction = QueryTriggerInteraction.Collide;
    private void OnEnable()
    {
        m_ProjectileBase = GetComponent<ProjectileBase>();
        particlesTransform = transform.Find("Particles");
        lineRenderer = GetComponent<LineRenderer>();

        m_ProjectileBase.onShoot += OnShoot;

        timer = timeBeforeDestroy;

        if (lineMaterial)
        {
            lineRenderer.material = lineMaterial;
        }
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f) DestroyProjectile();
    }

    void OnShoot()
    {
        m_RootPosition = transform.position;
        m_IgnoredColliders = new List<Collider>();

        RaycastHit closestHit = new RaycastHit();
        closestHit.distance = Mathf.Infinity;
        bool foundHit = false;

        // Ignore colliders of owner
        Collider[] ownerColliders = m_ProjectileBase.fromActor.GetComponentsInChildren<Collider>();
        m_IgnoredColliders.AddRange(ownerColliders);

        // Ray cast
        RaycastHit[] hits = Physics.RaycastAll(m_RootPosition, transform.forward, Mathf.Infinity, hittableLayers, k_TriggerInteraction);

        foreach (var hit in hits)
        {
            if (IsHitValid(hit) && hit.distance < closestHit.distance && hit.distance > 0f)
            {
                foundHit = true;
                closestHit = hit;
            }
        }

        Vector3[] positions;

        if (foundHit)
        {
            positions = new Vector3[2] { m_RootPosition, closestHit.point };
            OnHit(closestHit.point, closestHit.normal, closestHit.collider);
        }
        else
        {
            positions = new Vector3[2] { m_RootPosition, m_RootPosition + transform.forward * maxDistanceForInfiniteProjectile };
        }
        lineRenderer.SetPositions(positions);
    }

    bool IsHitValid(RaycastHit hit)
    {
        // ignore hits with an ignore component
        if (hit.collider.GetComponentInParent<IgnoreHitDetection>())
        {
            return false;
        }

        // ignore hits with triggers that don't have a Actor component
        if (hit.collider.isTrigger && hit.collider.GetComponentInParent<Actor>() == null)
        {
            return false;
        }

        // ignore hits with specific ignored colliders (self colliders, by default)
        if (m_IgnoredColliders.Contains(hit.collider))
        {
            return false;
        }

        return true;
    }

    void OnHit(Vector3 point, Vector3 normal, Collider collider)
    {
        
        // point damage
        Actor otherActor = collider.GetComponentInParent<Actor>();
        if (otherActor)
        {
            otherActor.InflictDamage(m_ProjectileBase.weaponDamage * m_ProjectileBase.fromActor.damageDoneMultiplier, m_ProjectileBase.fromActor);
        }

        // impact vfx
        if (impactVFX)
        {
            GameObject impactVFXInstance = Instantiate(impactVFX, point + (normal * impactVFXSpawnOffset), Quaternion.LookRotation(normal));
            DestroyOnParticleEnd impactSFXInstanceDestroyer = impactVFXInstance.GetComponent<DestroyOnParticleEnd>();
            if (impactSFXInstanceDestroyer == null) Destroy(impactVFXInstance, 5f);
        }

        // impact sfx
        if (impactSFXClip)
        {
            //AudioUtility.CreateSFX(impactSFXClip, point, AudioUtility.AudioGroups.Impact, 1f, 3f);
        }
    }

    void DestroyProjectile()
    {
        if (particlesTransform != null)
        {
            particlesTransform.SetParent(null);
            ParticleSystem ps = particlesTransform.GetComponent<ParticleSystem>();
            ps.Stop();
            Destroy(particlesTransform.gameObject, ps.main.startLifetime.constantMax);
        }
        Destroy(this.gameObject);
    }
}
