﻿using UnityEngine;
using UnityEngine.Events;

public class ProjectileBase : MonoBehaviour
{
    public float weaponDamage { get; private set; }
    public float projectileSpeed { get; private set; }
    public Actor fromActor { get; private set; }
    public Vector3 initialPosition { get; private set; }
    public Vector3 initialDirection { get; private set; }
    public Vector3 inheritedMuzzleVelocity { get; private set; }
    public float initialCharge { get; private set; }

    public UnityAction onShoot;

    public void Shoot(WeaponController weaponController, Actor actorController, float speed)
    {
        fromActor = actorController;
        projectileSpeed = speed;
        weaponDamage = weaponController.damage;
        initialPosition = transform.position;
        initialDirection = transform.forward;

        if (onShoot != null)
        {
            onShoot.Invoke();
        }
    }
}
