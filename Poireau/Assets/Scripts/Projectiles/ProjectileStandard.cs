﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileStandard : MonoBehaviour
{
    [Header("General")]
    [Tooltip("Radius of this projectile's collision detection")]
    public float radius = 0.01f;
    [Tooltip("LifeTime of the projectile")]
    public float maxLifeTime = 5f;
    [Tooltip("VFX prefab to spawn upon impact")]
    public GameObject impactVFX;
    [Tooltip("Offset along the hit normal where the VFX will be spawned")]
    public float impactVFXSpawnOffset = 0.1f;
    [Tooltip("Clip to play on impact")]
    public AudioClip impactSFXClip;
    [Tooltip("Layers this projectile can collide with")]
    public LayerMask hittableLayers = -1;

    [Header("Movement")]
    [Tooltip("Downward acceleration from gravity")]
    public float gravityDownAcceleration = 0f;

    ProjectileBase m_ProjectileBase;
    Transform particlesTransform;
    Vector3 m_LastRootPosition;
    Vector3 m_Velocity;
    Transform root;
    Transform tip;
    List<Collider> m_IgnoredColliders;

    const QueryTriggerInteraction k_TriggerInteraction = QueryTriggerInteraction.Collide;
    private void OnEnable()
    {
        root = transform;
        tip = transform.Find("Tip");
        m_ProjectileBase = GetComponent<ProjectileBase>();
        particlesTransform = transform.Find("Particles");

        m_ProjectileBase.onShoot += OnShoot;

        Destroy(gameObject, maxLifeTime);
    }

    void Update()
    {
        // Move
        transform.position += m_Velocity * Time.deltaTime;

        // Orient towards velocity
        transform.forward = m_Velocity.normalized;

        // Gravity
        if (gravityDownAcceleration > 0)
        {
            // add gravity to the projectile velocity for ballistic effect
            m_Velocity += Vector3.down * gravityDownAcceleration * Time.deltaTime;
        }

        // Hit detection
        {
            RaycastHit closestHit = new RaycastHit();
            closestHit.distance = Mathf.Infinity;
            bool foundHit = false;

            // Sphere cast
            Vector3 displacementSinceLastFrame = tip.position - m_LastRootPosition;
            RaycastHit[] hits = Physics.SphereCastAll(m_LastRootPosition, radius, displacementSinceLastFrame.normalized, displacementSinceLastFrame.magnitude, hittableLayers, k_TriggerInteraction);
            foreach (var hit in hits)
            {
                if (IsHitValid(hit) && hit.distance < closestHit.distance)
                {
                    foundHit = true;
                    closestHit = hit;
                }
            }

            if (foundHit)
            {
                // Handle case of casting while already inside a collider
                if (closestHit.distance <= 0f)
                {
                    closestHit.point = root.position;
                    closestHit.normal = -transform.forward;
                }

                OnHit(closestHit.point, closestHit.normal, closestHit.collider);
            }
        }

        m_LastRootPosition = root.position;
    }

    void OnShoot()
    {
        m_LastRootPosition = root.position;
        m_Velocity = transform.forward * m_ProjectileBase.projectileSpeed;
        m_IgnoredColliders = new List<Collider>();
        transform.position += m_ProjectileBase.inheritedMuzzleVelocity * Time.deltaTime;

        // Ignore colliders of owner
        Collider[] ownerColliders = m_ProjectileBase.fromActor.GetComponentsInChildren<Collider>();
        m_IgnoredColliders.AddRange(ownerColliders);
    }

    bool IsHitValid(RaycastHit hit)
    {
        // ignore hits with an ignore component
        if (hit.collider.GetComponentInParent<IgnoreHitDetection>())
        {
            return false;
        }

        // ignore hits with triggers that don't have a Actor component
        if (hit.collider.isTrigger && hit.collider.GetComponentInParent<Actor>() == null)
        {
            return false;
        }

        // ignore hits with specific ignored colliders (self colliders, by default)
        if (m_IgnoredColliders.Contains(hit.collider))
        {
            return false;
        }

        return true;
    }

    void OnHit(Vector3 point, Vector3 normal, Collider collider)
    {
        // point damage
        Actor otherActor = collider.GetComponentInParent<Actor>();
        if (otherActor)
        {
            otherActor.InflictDamage(m_ProjectileBase.weaponDamage * m_ProjectileBase.fromActor.damageDoneMultiplier, m_ProjectileBase.fromActor);
        }

        // impact vfx
        if (impactVFX)
        {
            GameObject impactVFXInstance = Instantiate(impactVFX, point + (normal * impactVFXSpawnOffset), Quaternion.LookRotation(normal));
            DestroyOnParticleEnd impactSFXInstanceDestroyer = impactVFXInstance.GetComponent<DestroyOnParticleEnd>();
            if (impactSFXInstanceDestroyer == null) Destroy(impactVFXInstance, 5f);
        }

        // impact sfx
        if (impactSFXClip)
        {
            //AudioUtility.CreateSFX(impactSFXClip, point, AudioUtility.AudioGroups.Impact, 1f, 3f);
        }

        // Self Destruct
        DestroyProjectile();
    }

    void DestroyProjectile()
    {
        if (particlesTransform != null)
        {
            particlesTransform.SetParent(null);
            ParticleSystem ps = particlesTransform.GetComponent<ParticleSystem>();
            ps.Stop();
            Destroy(particlesTransform.gameObject, ps.main.startLifetime.constantMax);
        }
        Destroy(this.gameObject);
    }
}
