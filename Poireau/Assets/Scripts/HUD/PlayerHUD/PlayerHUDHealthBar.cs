﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDHealthBar : MonoBehaviour
{
    Image mask;
    float originalSize;

    void Awake()
    {
        mask = transform.Find("HealthContainer").Find("Mask").gameObject.GetComponent<Image>();
    }
    void Start()
    {
        originalSize = mask.rectTransform.rect.width;
    }

    public void SetValue(float value)
    {
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }
}
