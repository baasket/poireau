﻿using UnityEngine;
using UnityEngine.UI;

public class AIHUDHealthBar : MonoBehaviour
{
    Image mask;
    float originalSize;
    PlayerActor playerActor;
    void Awake()
    {
        mask = transform.Find("Container").Find("Mask").gameObject.GetComponent<Image>();
    }
    void Start()
    {
        originalSize = mask.rectTransform.rect.width;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
    }

    public void SetValue(float value)
    {
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }
}
