﻿using UnityEngine;
using UnityEngine.UI;

public class AIHUDShieldBar : MonoBehaviour
{
    Image mask;
    float originalSize;
    void Awake()
    {
        mask = transform.Find("Container").Find("Mask").gameObject.GetComponent<Image>();
    }
    void Start()
    {
        originalSize = mask.rectTransform.rect.width;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
    }

    public void SetValue(float value)
    {
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }
}
