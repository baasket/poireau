﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHUDBase : MonoBehaviour
{
    AIHUDHealthBar healthBar;
    AIHUDShieldBar shieldBar;
    void Awake()
    {
        healthBar = GetComponentInChildren<AIHUDHealthBar>();
        shieldBar = GetComponentInChildren<AIHUDShieldBar>();
    }
    public void SetHealthValue(float value)
    {
        healthBar.SetValue(value);
    }
    public void SetShieldValue(float value)
    {
        shieldBar.SetValue(value);
        if (value <= 0f)
        {
            shieldBar.gameObject.SetActive(false);
        }
        else
        {
            shieldBar.gameObject.SetActive(true);
        }
    }
}
