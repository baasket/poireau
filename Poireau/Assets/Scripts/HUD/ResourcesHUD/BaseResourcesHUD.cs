﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseResourcesHUD : MonoBehaviour
{
    Text c_text;

    private void Awake()
    {
        c_text = GetComponentInChildren<Text>();
    }

    void SetText(string text)
    {
        c_text.text = text;
    }

    public void DisplayResource(string resource_name, int quantity)
    {
        SetText(quantity + " " + resource_name);
    }
}
