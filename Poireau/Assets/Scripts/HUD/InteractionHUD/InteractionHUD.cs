﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class InteractionHUD : MonoBehaviour
{
    public SimpleText simpleText;
    public List<AbstractInteraction> interactions = new List<AbstractInteraction>();

    private void Awake()
    {
        // Simple text
        simpleText = new SimpleText(GetChildGameObject("InteractionText"));
        interactions.Add(simpleText);

        // Be sure all GO are inactive
        ClearAllInteraction();
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.Rotate(new Vector3(0, 180, 0));
    }

    private GameObject GetChildGameObject(string goName)
    {
        return transform.Find(goName).gameObject;
    }

    public void ClearAllInteraction()
    {
        foreach (AbstractInteraction interaction in interactions)
        {
            if (interaction.interactionGO.activeInHierarchy)
            {
                interaction.Clear();
            }
        }
    }
}

public abstract class AbstractInteraction
{
    public GameObject interactionGO;
    public abstract void Clear();
}

public class SimpleText : AbstractInteraction
{
    public Text simpleText;

    public SimpleText(GameObject gameobject)
    {
        interactionGO = gameobject;
        simpleText = interactionGO.GetComponentInChildren<Text>();
    }
    public void SetSimpleText(string text)
    {
        interactionGO.SetActive(true);
        simpleText.text = text;
    }

    public override void Clear()
    {
        interactionGO.SetActive(false);
        simpleText.text = "";
    }
}

