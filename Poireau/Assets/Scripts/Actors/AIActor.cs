﻿using UnityEngine;

public class AIActor : Actor
{
    [Range(0, 1)]
    public float chanceDrop = 0.5f;

    AbstractAIController aiController;
    AIHUDBase HUDBase;
    bool killed = false;
    protected override void Start()
    {
        base.Start();
        aiController = GetComponent<AbstractAIController>();
        HUDBase = GetComponentInChildren<AIHUDBase>();
    }

    protected override void ChangeHealth(float point, Actor fromActor)
    {
        base.ChangeHealth(point, fromActor);
        if (HUDBase) HUDBase.SetHealthValue(health / (float)maxHealth);
    }

    public override void InflictDamage(float damage, Actor fromActor)
    {
        base.InflictDamage(damage, fromActor);
        if (aiController != null && fromActor.group != group && health > 0)
        {
            aiController.FocusActor(fromActor);
        }
    }

    public override void Kill()
    {
        if (killed) return;
        killed = true;
        base.Kill();
        Destroy(gameObject);
    }
}
