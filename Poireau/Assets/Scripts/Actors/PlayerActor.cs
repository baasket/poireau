﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(CharacterController))]
public class PlayerActor : Actor
{
    public PlayerController playerController { get; private set; }

    public int id;

    [HideInInspector]
    public CharacterController characterController;

    public PlayerHUDHealthBar healthHUD;

    [HideInInspector]
    public InteractionHUD interactionHUD;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        interactionHUD = GetComponentInChildren<InteractionHUD>();

        PlayerManager.Register(this);
    }
    protected override void ChangeHealth(float point, Actor fromActor)
    {
        base.ChangeHealth(point, fromActor);
        healthHUD.SetValue(health / (float)maxHealth);
    }

    public override void Kill()
    {
        if (!GameManager.IsLevelRunning()) return;
        base.Kill();
        GameManager.levelState = GameConstants.s_LevelFail;
    }
}
