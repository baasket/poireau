﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIActor))]
public class ActorObserver : MonoBehaviour
{
    public float observeDistance = 10f;

    Actor actor;
    Transform viewPoint;
    void Start()
    {
        actor = GetComponent<Actor>();
        viewPoint = transform.Find("ViewPoint");
    }

    bool ContainValidActor(Collider collider, out Actor outActor)
    {
        outActor = collider.gameObject.GetComponent<Actor>();
        if (outActor == null || transform == outActor.transform)
        {
            outActor = null;
            return false;
        }
        return true;
    }

    public Vector3 GetActorHitPosition(Actor otherActor)
    {
        Transform otherActorHitPoint = otherActor.transform.Find("HitPoint");
        if (otherActorHitPoint) return otherActorHitPoint.position;
        Transform otherActorViewPoint = otherActor.transform.Find("ViewPoint");
        if (otherActorViewPoint) return otherActorViewPoint.position;
        return otherActor.GetComponentInChildren<Renderer>().bounds.center;
    }

    public bool DoSeeActor(Actor actorFocused, out float sqrDistance, out Vector3 otherActorHitPosition)
    {
        otherActorHitPosition = GetActorHitPosition(actorFocused);
        sqrDistance = (otherActorHitPosition - viewPoint.position).sqrMagnitude;
        RaycastHit raycastHit;
        return Physics.Raycast
            (viewPoint.position,
            otherActorHitPosition - viewPoint.position,
            out raycastHit,
            observeDistance,
            Physics.DefaultRaycastLayers,
            QueryTriggerInteraction.Ignore
            ) && raycastHit.collider.transform == actorFocused.transform;
    }

    public bool DoSeeActor(Actor actorFocused, out float sqrDistance)
    {
        return DoSeeActor(actorFocused, out sqrDistance, out _);
    }

    public bool DoSeeActor(Actor actorFocused)
    {
        return DoSeeActor(actorFocused, out _, out _);
    }

    public Actor FindFirstActor(bool seeActor = true, bool detectAllyActor = false)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, observeDistance);

        Actor otherActor;
        foreach (Collider collider in colliders)
        {
            if (
                !ContainValidActor(collider, out otherActor) ||
                (seeActor && !DoSeeActor(otherActor)) ||
                (!detectAllyActor && otherActor.group == actor.group)
                ) continue;
            return otherActor;
        }
        return null;
    }

    public Actor FindNearestActor(bool seeActor = true, bool detectAllyActor = false)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, observeDistance);
        float nearestActorDistance = Mathf.Infinity;
        Actor nearestActorObject = null;
        Actor otherActor;
        foreach (Collider collider in colliders)
        {
            float sqrDistance;
            if (
                !ContainValidActor(collider, out otherActor) ||
                (!detectAllyActor && otherActor.group == actor.group)
                ) continue;
            if (!DoSeeActor(otherActor, out sqrDistance) && seeActor) continue;
            if (sqrDistance < nearestActorDistance)
            {
                nearestActorObject = otherActor;
                nearestActorDistance = sqrDistance;
            }
        }
        return nearestActorObject;

    }

    public Actor[] FindAllActors(bool seeActor = true, bool detectAllyActor = false)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, observeDistance);
        List<Actor> actors = new List<Actor>();
        Actor otherActor;
        foreach (Collider collider in colliders)
        {
            if (
                !ContainValidActor(collider, out otherActor) ||
                (seeActor && !DoSeeActor(otherActor)) ||
                (!detectAllyActor && otherActor.group == actor.group)
                ) continue;
            actors.Add(otherActor);
        }
        return actors.ToArray();

    }

    public Actor FindSpecificActor(Actor actorToFind, bool seeActor = true)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, observeDistance);

        Actor otherActor;
        foreach (Collider collider in colliders)
        {
            if (
                !ContainValidActor(collider, out otherActor) ||
                (seeActor && !DoSeeActor(otherActor)) ||
                (otherActor != actorToFind)
                ) continue;
            return otherActor;
        }
        return null;
    }
}
