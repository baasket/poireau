﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{
    public GameConstants.ActorType actorType;
    public int maxHealth = 10;
    public bool isAttackable = true;
    public int group = 0;
    public float damageTakenMultiplier = 1f;
    public float damageDoneMultiplier = 1f;
    public float attackSpeedMultiplier = 1f;
    public int dieUnderYLevel = -10;

    public AudioClip deathClip;
    protected float health;

    protected float actorRadius;
    protected Vector3 actorCenter;

    void Update()
    {
        if (transform.position.y < dieUnderYLevel)
        {
            Kill();
        }
    }

    protected virtual void Start()
    {
        health = maxHealth;
        if (!isAttackable) return;
        ActorManager.AddActor(this);

        Renderer[] actorRenderers = GetComponentsInChildren<Renderer>();
        Vector3 center = transform.position;
        float magnitudeMax = 0f;
        foreach (Renderer ren in actorRenderers)
        {
            float tmpRadius = ren.bounds.extents.magnitude;
            if (tmpRadius < magnitudeMax) continue;
            magnitudeMax = tmpRadius;
            center = ren.bounds.center;
        }

        actorRadius = magnitudeMax;
        actorCenter = center;
    }

    protected virtual void ChangeHealth(float point, Actor fromActor)
    {
        if (!GameManager.IsLevelRunning() || !isAttackable) return;
        health = Mathf.Clamp((float)System.Math.Round(health + point, 2), 0, maxHealth);
        if (Mathf.Approximately(health, 0)) Kill();
    }

    public virtual void InflictDamage(float damage, Actor fromActor)
    {
        if (damage < 0) return;
        ChangeHealth(-damage, fromActor);
    }

    public virtual bool Heal(float healthPoint, Actor fromActor)
    {
        if (healthPoint < 0 || health >= maxHealth) return false;
        ChangeHealth(healthPoint, fromActor);
        return true;
    }

    public virtual void Kill()
    {
        if (!GameManager.IsLevelRunning()) return;
        ActorManager.RemoveActor(this);
        if (deathClip) AudioSource.PlayClipAtPoint(deathClip, transform.position);
    }
}
